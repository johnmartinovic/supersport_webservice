SELECT Sportovi.Naziv, Lige.Naziv,
 VrsteTipova.Naziv, VrsteTipova.VrsteTipovaArr,
 VrijednostiTipova.VrijednostiTipovaArr,
 DownloadedPonude.VrijemeDownloada, 
 Dogadaji.DatumPrikaza, Dogadaji.Broj, Dogadaji.Naziv, 
 Dogadaji.TipoviDobitniArr, Dogadaji.Rezultat, 
 Dogadaji.DodatanOpisDogadaja, Dogadaji.DetaljnoVrijemePocetka, 
 Dogadaji.WebVrijemePocetka, Dogadaji.DodatanOpisLige 
 FROM Dogadaji
  JOIN Lige ON Dogadaji.Lige_Id=Lige.Id
  JOIN Sportovi ON Lige.Sportovi_Id=Sportovi.Id
  JOIN VrsteTipova ON Dogadaji.VrsteTipova_Id=VrsteTipova.Id
  JOIN VrijednostiTipova ON VrijednostiTipova.Dogadaji_Id=Dogadaji.Id
  JOIN DownloadedPonude ON VrijednostiTipova.DownloadedPonude_Id=DownloadedPonude.Id 
 WHERE Dogadaji.DatumPrikaza = "2016-08-29" AND Dogadaji.Broj = 445;
 
 ***************
 SELECT 
  Sportovi.Naziv, Lige.Naziv,
  VrsteTipova.Naziv, VrsteTipova.VrsteTipovaArr,
  VrijednostiTipova.VrijednostiTipovaArr,
  DownloadedPonude.VrijemeDownloada, 
  Dogadaji.DatumPrikaza, Dogadaji.Broj, Dogadaji.Naziv, 
  Dogadaji.TipoviDobitniArr, Dogadaji.Rezultat, 
  Dogadaji.DodatanOpisDogadaja, Dogadaji.DetaljnoVrijemePocetka, 
  Dogadaji.WebVrijemePocetka, Dogadaji.DodatanOpisLige 
 FROM Dogadaji
  JOIN Lige ON Dogadaji.Lige_Id=Lige.Id
  JOIN Sportovi ON Lige.Sportovi_Id=Sportovi.Id
  JOIN VrsteTipova ON Dogadaji.VrsteTipova_Id=VrsteTipova.Id
  JOIN VrijednostiTipova ON VrijednostiTipova.Dogadaji_Id=Dogadaji.Id
  JOIN DownloadedPonude ON VrijednostiTipova.DownloadedPonude_Id=DownloadedPonude.Id 
 WHERE Dogadaji.Naziv='Sobotta P.-Dalby N.';

*****************
 SELECT
  Sportovi.Naziv, Lige.Naziv,
  VrsteTipova.Naziv, VrsteTipova.VrsteTipovaArr,
  Dogadaji.DatumPrikaza, Dogadaji.Broj, Dogadaji.Naziv, 
  Dogadaji.TipoviDobitniArr, Dogadaji.Rezultat, 
  Dogadaji.DodatanOpisDogadaja, Dogadaji.DetaljnoVrijemePocetka, 
  Dogadaji.WebVrijemePocetka, Dogadaji.DodatanOpisLige 
 FROM Dogadaji
  JOIN Lige ON Dogadaji.Lige_Id=Lige.Id
  JOIN Sportovi ON Lige.Sportovi_Id=Sportovi.Id
  JOIN VrsteTipova ON Dogadaji.VrsteTipova_Id=VrsteTipova.Id
 WHERE Dogadaji.Naziv='Sobotta P.-Dalby N.';
 
 SELECT
  VrijednostiTipova.VrijednostiTipovaArr,
  DownloadedPonude.VrijemeDownloada
 FROM VrijednostiTipova
  JOIN DownloadedPonude ON VrijednostiTipova.DownloadedPonude_Id=DownloadedPonude.Id 
 WHERE VrijednostiTipova.Dogadaji_Id=1501;
 *****************