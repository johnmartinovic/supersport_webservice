package com.johnniem.SuperSportWebService;

import java.sql.Timestamp;

public class Tipovi {
	
	private String vrijednostiTipova;
	private Timestamp vrijemeDownloada;

	public Tipovi() {
	}

	public Tipovi(String vrijednostiTipova, Timestamp vrijemeDownloada) {
		this.vrijednostiTipova = vrijednostiTipova;
		this.vrijemeDownloada = vrijemeDownloada;
	}
	
	public String getVrijednostiTipova() {
		return vrijednostiTipova;
	}
	
	public void setVrijednostiTipova(String vrijednostiTipova) {
		this.vrijednostiTipova = vrijednostiTipova;
	}
	
	public Timestamp getVrijemeDownloada() {
		return vrijemeDownloada;
	}
	
	public void setVrijemeDownloada(Timestamp vrijemeDownloada) {
		this.vrijemeDownloada = vrijemeDownloada;
	}
}
