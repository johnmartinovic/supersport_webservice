package com.johnniem.SuperSportWebService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.johnniem.SuperSportWebService.DogadajPOJOs.Dogadaj;
import com.johnniem.SuperSportWebService.MySqlDAO.DogadajJDBCDAO;

@Repository
public class DogadajJDBCDAOImpl implements DogadajJDBCDAO {

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
	
	@Override
	public List<Dogadaj> getDogadajList(Date datumPrikaza) {
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        String strDatumPrikaza = dateFormat.format(datumPrikaza);
		
		String sql = 
				" SELECT\n" + 
				"  Sportovi.Naziv, Lige.Naziv,\n" + 
				"  VrsteTipova.Naziv, VrsteTipova.VrsteTipovaArr,\n" + 
				"  Dogadaji.Id, Dogadaji.DatumPrikaza, Dogadaji.Broj, Dogadaji.Naziv, \n" + 
				"  Dogadaji.TipoviDobitniArr, Dogadaji.Rezultat, \n" + 
				"  Dogadaji.DodatanOpisDogadaja, Dogadaji.DetaljnoVrijemePocetka, \n" + 
				"  Dogadaji.WebVrijemePocetka, Dogadaji.DodatanOpisLige \n" + 
				" FROM Dogadaji\n" + 
				"  JOIN Lige ON Dogadaji.Lige_Id=Lige.Id\n" + 
				"  JOIN Sportovi ON Lige.Sportovi_Id=Sportovi.Id\n" + 
				"  JOIN VrsteTipova ON Dogadaji.VrsteTipova_Id=VrsteTipova.Id" + 
				" WHERE Dogadaji.DatumPrikaza = '" + strDatumPrikaza + "';";
		
		List<Dogadaj> dogadajList = jdbcTemplate.query(
				sql,
				new RowMapper<Dogadaj>() {
					@Override
					public Dogadaj mapRow(ResultSet rs, int rowNum) throws SQLException {
						Dogadaj dogadaj;
				
						dogadaj = new Dogadaj(
							rs.getString(1),
							rs.getString(2),
							rs.getString(3),
							rs.getString(4),
							rs.getInt(5),
							rs.getDate(6),
							rs.getString(7),
							rs.getString(8),
							rs.getString(9),
							rs.getString(10),
							rs.getString(11),
							rs.getString(12),
							rs.getString(13),
							rs.getString(14)
							);
						
						return dogadaj;
					}
				}
		);
		
		return dogadajList;
	}
	
	@Override
	public Dogadaj getDogadaj(Date datumPrikaza, String broj) {
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        String strDatumPrikaza = dateFormat.format(datumPrikaza);
		
		String sql = 
				" SELECT\n" + 
				"  Sportovi.Naziv, Lige.Naziv,\n" + 
				"  VrsteTipova.Naziv, VrsteTipova.VrsteTipovaArr,\n" + 
				"  Dogadaji.Id, Dogadaji.DatumPrikaza, Dogadaji.Broj, Dogadaji.Naziv, \n" + 
				"  Dogadaji.TipoviDobitniArr, Dogadaji.Rezultat, \n" + 
				"  Dogadaji.DodatanOpisDogadaja, Dogadaji.DetaljnoVrijemePocetka, \n" + 
				"  Dogadaji.WebVrijemePocetka, Dogadaji.DodatanOpisLige \n" + 
				" FROM Dogadaji\n" + 
				"  JOIN Lige ON Dogadaji.Lige_Id=Lige.Id\n" + 
				"  JOIN Sportovi ON Lige.Sportovi_Id=Sportovi.Id\n" + 
				"  JOIN VrsteTipova ON Dogadaji.VrsteTipova_Id=VrsteTipova.Id" + 
				" WHERE Dogadaji.DatumPrikaza = '" + strDatumPrikaza + "'" +
				"  AND Dogadaji.Broj = '" + broj + "';";
		
		List<Dogadaj> dogadajList = jdbcTemplate.query(
				sql,
				new RowMapper<Dogadaj>() {
					@Override
					public Dogadaj mapRow(ResultSet rs, int rowNum) throws SQLException {
						Dogadaj dogadaj;
				
						dogadaj = new Dogadaj(
							rs.getString(1),
							rs.getString(2),
							rs.getString(3),
							rs.getString(4),
							rs.getInt(5),
							rs.getDate(6),
							rs.getString(7),
							rs.getString(8),
							rs.getString(9),
							rs.getString(10),
							rs.getString(11),
							rs.getString(12),
							rs.getString(13),
							rs.getString(14)
							);
						
						return dogadaj;
					}
				}
		);
		
		// there should be only one dogadaj with specified parameters
		if (dogadajList.size() != 1)
			return new Dogadaj();
		else
			return dogadajList.get(0);
	}
	
	@Override
	public List<Tipovi> getDogadajTipoviList(int dogadajId) {
		
		String sql = 
				" SELECT\n" + 
				"  VrijednostiTipova.VrijednostiTipovaArr,\n" + 
				"  DownloadedPonude.VrijemeDownloada\n" + 
				" FROM VrijednostiTipova\n" + 
				"  JOIN DownloadedPonude ON VrijednostiTipova.DownloadedPonude_Id=DownloadedPonude.Id \n" + 
				" WHERE VrijednostiTipova.Dogadaji_Id = ?;";
		
		List<Tipovi> tipoviList = jdbcTemplate.query(
				sql,
				new Object[] { dogadajId },
				new RowMapper<Tipovi>() {
					@Override
					public Tipovi mapRow(ResultSet rs, int rowNum) throws SQLException {
						Tipovi tipovi;
				
						tipovi = new Tipovi(
								rs.getString(1),
								rs.getTimestamp(2)
							);
						
						return tipovi;
					}
				}
		);
		
		return tipoviList;
	}
	
	@Override
	public List<Tipovi> getDogadajTipoviList(Date datumPrikaza, String broj) {
		
		Dogadaj dogadaj = getDogadaj(datumPrikaza, broj);
		
		List<Tipovi> tipoviList = getDogadajTipoviList(dogadaj.getId());
		
		return tipoviList;
	}
	
	/*
	public List<DogadajWithTipovi> getDogadajInfoByDate(Date date) {
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        String strDate = dateFormat.format(date);
		
		String sql = "SELECT Sportovi.Naziv, Lige.Naziv,\n" + 
				" VrsteTipova.Naziv, VrsteTipova.VrsteTipovaArr,\n" + 
				" VrijednostiTipova.VrijednostiTipovaArr,\n" + 
				" DownloadedPonude.VrijemeDownloada, \n" + 
				" Dogadaji.DatumPrikaza, Dogadaji.Broj, Dogadaji.Naziv, \n" + 
				" Dogadaji.TipoviDobitniArr, Dogadaji.Rezultat, \n" + 
				" Dogadaji.DodatanOpisDogadaja, Dogadaji.DetaljnoVrijemePocetka, \n" + 
				" Dogadaji.WebVrijemePocetka, Dogadaji.DodatanOpisLige \n" + 
				" FROM Dogadaji\n" + 
				"  JOIN Lige ON Dogadaji.Lige_Id=Lige.Id\n" + 
				"  JOIN Sportovi ON Lige.Sportovi_Id=Sportovi.Id\n" + 
				"  JOIN VrsteTipova ON Dogadaji.VrsteTipova_Id=VrsteTipova.Id\n" + 
				"  JOIN VrijednostiTipova ON VrijednostiTipova.Dogadaji_Id=Dogadaji.Id\n" + 
				"  JOIN DownloadedPonude ON VrijednostiTipova.DownloadedPonude_Id=DownloadedPonude.Id \n" + 
				" WHERE Dogadaji.DatumPrikaza = '" + strDate + "';";
		
		List<DogadajJedanTip> dogadajJedanTipList = jdbcTemplate.query(
				sql,
				new RowMapper<DogadajJedanTip>() {
					@Override
					public DogadajJedanTip mapRow(ResultSet rs, int rowNum) throws SQLException {
						DogadajJedanTip dogadajJedanTip;
				
						dogadajJedanTip = new DogadajJedanTip(
								new Dogadaj(
										rs.getString(1),
										rs.getString(2),
										rs.getString(3),
										rs.getString(4),
										rs.getDate(7),
										rs.getString(8),
										rs.getString(9),
										rs.getString(10),
										rs.getString(11),
										rs.getString(12),
										rs.getString(13),
										rs.getString(14),
										rs.getString(15)
										),
								rs.getTimestamp(6),
								rs.getString(5)
							);
						
						return dogadajJedanTip;
					}
				}
		);
		
		Map<Dogadaj, List<Tipovi>> dogadajWithTipoviListMap = new HashMap<>();
		
		for (DogadajJedanTip dogadajJedanTip : dogadajJedanTipList) {
			
			dogadajWithTipoviListMap.merge(
					dogadajJedanTip,
					new LinkedList<Tipovi>(Arrays.asList(dogadajJedanTip.getTipovi())),
					(oldValue, value) -> {
						oldValue.add(dogadajJedanTip.getTipovi());
						return oldValue;
					}
			);
		}
		
		List<DogadajWithTipovi> dogadajWithTipoviList = new LinkedList<>();
		
		for (Map.Entry<Dogadaj, List<Tipovi>> entry : dogadajWithTipoviListMap.entrySet()) {
			dogadajWithTipoviList.add(new DogadajWithTipovi(entry.getKey(), entry.getValue()));
		}
		
		return dogadajWithTipoviList;
	}

	
	public List<FullDogadaj> getFullDogadajInfoByName(String naziv) {
		
		String sql = "SELECT Sportovi.Naziv, Lige.Naziv,\n" + 
				" VrsteTipova.Naziv, VrsteTipova.VrsteTipovaArr,\n" + 
				" VrijednostiTipova.VrijednostiTipovaArr,\n" + 
				" DownloadedPonude.VrijemeDownloada, \n" + 
				" Dogadaji.DatumPrikaza, Dogadaji.Broj, Dogadaji.Naziv, \n" + 
				" Dogadaji.TipoviDobitniArr, Dogadaji.Rezultat, \n" + 
				" Dogadaji.DodatanOpisDogadaja, Dogadaji.DetaljnoVrijemePocetka, \n" + 
				" Dogadaji.WebVrijemePocetka, Dogadaji.DodatanOpisLige \n" + 
				" FROM Dogadaji\n" + 
				"  JOIN Lige ON Dogadaji.Lige_Id=Lige.Id\n" + 
				"  JOIN Sportovi ON Lige.Sportovi_Id=Sportovi.Id\n" + 
				"  JOIN VrsteTipova ON Dogadaji.VrsteTipova_Id=VrsteTipova.Id\n" + 
				"  JOIN VrijednostiTipova ON VrijednostiTipova.Dogadaji_Id=Dogadaji.Id\n" + 
				"  JOIN DownloadedPonude ON VrijednostiTipova.DownloadedPonude_Id=DownloadedPonude.Id \n" + 
				" WHERE Dogadaji.Naziv LIKE '" + naziv + "%';";
		
		List<FullDogadaj> fullDogadajList = jdbcTemplate.query(
				sql,
				new RowMapper<FullDogadaj>() {
					@Override
					public FullDogadaj mapRow(ResultSet rs, int rowNum) throws SQLException {
						FullDogadaj fullDogadaj;
				
						fullDogadaj = new FullDogadaj(
							rs.getString(1),
							rs.getString(2),
							rs.getString(3),
							rs.getString(4),
							new Tipovi(
									rs.getString(5),
									rs.getTimestamp(6)
									),
							rs.getDate(7),
							rs.getString(8),
							rs.getString(9),
							rs.getString(10),
							rs.getString(11),
							rs.getString(12),
							rs.getString(13),
							rs.getString(14),
							rs.getString(15)
							);
						
						return fullDogadaj;
					}
				}
		);
		
		return fullDogadajList;
	}
	
	public List<Dogadaj> getDogadajInfoByName(String naziv) {
		
		String sql = 
				" SELECT\n" + 
				"  Sportovi.Naziv, Lige.Naziv,\n" + 
				"  VrsteTipova.Naziv, VrsteTipova.VrsteTipovaArr,\n" + 
				"  Dogadaji.DatumPrikaza, Dogadaji.Broj, Dogadaji.Naziv, \n" + 
				"  Dogadaji.TipoviDobitniArr, Dogadaji.Rezultat, \n" + 
				"  Dogadaji.DodatanOpisDogadaja, Dogadaji.DetaljnoVrijemePocetka, \n" + 
				"  Dogadaji.WebVrijemePocetka, Dogadaji.DodatanOpisLige \n" + 
				" FROM Dogadaji\n" + 
				"  JOIN Lige ON Dogadaji.Lige_Id=Lige.Id\n" + 
				"  JOIN Sportovi ON Lige.Sportovi_Id=Sportovi.Id\n" + 
				"  JOIN VrsteTipova ON Dogadaji.VrsteTipova_Id=VrsteTipova.Id" + 
				" WHERE Dogadaji.Naziv LIKE '" + naziv + "%';";
		
		List<Dogadaj> dogadajList = jdbcTemplate.query(
				sql,
				new RowMapper<Dogadaj>() {
					@Override
					public Dogadaj mapRow(ResultSet rs, int rowNum) throws SQLException {
						Dogadaj dogadaj;
				
						dogadaj = new Dogadaj(
							rs.getString(1),
							rs.getString(2),
							rs.getString(3),
							rs.getString(4),
							rs.getDate(5),
							rs.getString(6),
							rs.getString(7),
							rs.getString(8),
							rs.getString(9),
							rs.getString(10),
							rs.getString(11),
							rs.getString(12),
							rs.getString(13)
							);
						
						return dogadaj;
					}
				}
		);
		
		return dogadajList;
	}
	

	public List<DogadajWithDogadaji_Id> getDogadajWithIdInfoByName(String naziv) {
		
		String sql = 
				" SELECT\n" + 
				"  Sportovi.Naziv, Lige.Naziv,\n" + 
				"  VrsteTipova.Naziv, VrsteTipova.VrsteTipovaArr,\n" + 
				"  Dogadaji.Id, Dogadaji.DatumPrikaza, Dogadaji.Broj, Dogadaji.Naziv, \n" + 
				"  Dogadaji.TipoviDobitniArr, Dogadaji.Rezultat, \n" + 
				"  Dogadaji.DodatanOpisDogadaja, Dogadaji.DetaljnoVrijemePocetka, \n" + 
				"  Dogadaji.WebVrijemePocetka, Dogadaji.DodatanOpisLige \n" + 
				" FROM Dogadaji\n" + 
				"  JOIN Lige ON Dogadaji.Lige_Id=Lige.Id\n" + 
				"  JOIN Sportovi ON Lige.Sportovi_Id=Sportovi.Id\n" + 
				"  JOIN VrsteTipova ON Dogadaji.VrsteTipova_Id=VrsteTipova.Id" + 
				" WHERE Dogadaji.Naziv = '" + naziv + "';";
		
		List<DogadajWithDogadaji_Id> dogadajWithDogadaji_IdList = jdbcTemplate.query(
				sql,
				new RowMapper<DogadajWithDogadaji_Id>() {
					@Override
					public DogadajWithDogadaji_Id mapRow(ResultSet rs, int rowNum) throws SQLException {
						DogadajWithDogadaji_Id dogadajWithDogadaji_Id;
				
						dogadajWithDogadaji_Id = new DogadajWithDogadaji_Id(
							rs.getString(1),
							rs.getString(2),
							rs.getString(3),
							rs.getString(4),
							rs.getInt(5),
							rs.getDate(6),
							rs.getString(7),
							rs.getString(8),
							rs.getString(9),
							rs.getString(10),
							rs.getString(11),
							rs.getString(12),
							rs.getString(13),
							rs.getString(14)
							);
						
						return dogadajWithDogadaji_Id;
					}
				}
		);
		
		return dogadajWithDogadaji_IdList;
	}

	@Override
	public List<DogadajWithTipovi> getDogadajWithTipoviInfoByName(String naziv) {
		
		List<DogadajWithDogadaji_Id> dogadajWithDogadaji_IdList = getDogadajWithIdInfoByName(naziv);
		
		List<DogadajWithTipovi> dogadajWithTipoviList = new LinkedList<DogadajWithTipovi>();
		
		for (DogadajWithDogadaji_Id dogadajWithDogadaji_Id : dogadajWithDogadaji_IdList) {
			List<Tipovi> tipoviList = getTipoviByDogadajId(dogadajWithDogadaji_Id.getId());
			
			dogadajWithTipoviList.add(new DogadajWithTipovi(dogadajWithDogadaji_Id.getDogadaj(), tipoviList));
		}
		
		return dogadajWithTipoviList;
	}
	
	
	
	
	@Override
	public List<DogadajWithTipovi> getDogadajWithTipoviInfoByDate(Date date) {
		
		List<DogadajWithDogadaji_Id> dogadajWithDogadaji_IdList = getDogadajWithIdInfoByDate(date);
		
		List<DogadajWithTipovi> dogadajWithTipoviList = new LinkedList<DogadajWithTipovi>();
		
		for (DogadajWithDogadaji_Id dogadajWithDogadaji_Id : dogadajWithDogadaji_IdList) {
			List<Tipovi> tipoviList = getTipoviByDogadajId(dogadajWithDogadaji_Id.getId());
			
			dogadajWithTipoviList.add(new DogadajWithTipovi(new Dogadaj(dogadajWithDogadaji_Id), tipoviList));
		}
		
		return dogadajWithTipoviList;
	}
	
	*/

}
