package com.johnniem.SuperSportWebService.DogadajPOJOs;

import java.sql.Timestamp;

import com.johnniem.SuperSportWebService.Tipovi;

public class DogadajJedanTip extends Dogadaj {

	private Tipovi tipovi;
	
	public DogadajJedanTip() {
	}
	
	public DogadajJedanTip(Dogadaj dogadaj, Timestamp vrijemeDownloada, String vrijednostiTipova) {
		super(dogadaj.getSport(), dogadaj.getLiga(), dogadaj.getNazivVrsteTipova(), dogadaj.getVrsteTipova(), dogadaj.getId(),
				dogadaj.getDatumPrikaza(), dogadaj.getBroj(), dogadaj.getNaziv(), dogadaj.getDobitniTipovi(), dogadaj.getRezultat(),
				dogadaj.getDodatanOpisDogadaja(), dogadaj.getDetaljnoVrijemePocetka(), dogadaj.getWebVrijemePocetka(),
				dogadaj.getDodatanOpisLige());
		
		this.tipovi = new Tipovi(vrijednostiTipova, vrijemeDownloada);
	}

	public Tipovi getTipovi() {
		return tipovi;
	}

	public void setTipovi(Tipovi tipovi) {
		this.tipovi = tipovi;
	}

	
}