package com.johnniem.SuperSportWebService.DogadajPOJOs;

import java.util.Date;

public class Dogadaj {

	private String sport;
	private String liga;
	private String nazivVrsteTipova;
	private String vrsteTipova;
	private int id;
	private Date datumPrikaza;
	private String broj;
	private String naziv;
	private String dobitniTipovi;
	private String rezultat;
	private String dodatanOpisDogadaja;
	private String detaljnoVrijemePocetka;
	private String webVrijemePocetka;
	private String dodatanOpisLige;

	public Dogadaj() {
	}
	
	public Dogadaj(String sport, String liga, String nazivVrsteTipova, String vrsteTipova, int id,
			Date datumPrikaza, String broj, String naziv, String dobitniTipovi, String rezultat,
			String dodatanOpisDogadaja, String detaljnoVrijemePocetka, String webVrijemePocetka,
			String dodatanOpisLige) {
		this.sport = sport;
		this.liga = liga;
		this.nazivVrsteTipova = nazivVrsteTipova;
		this.vrsteTipova = vrsteTipova;
		this.id = id;
		this.datumPrikaza = datumPrikaza;
		this.broj = broj;
		this.naziv = naziv;
		this.dobitniTipovi = dobitniTipovi;
		this.rezultat = rezultat;
		this.dodatanOpisDogadaja = dodatanOpisDogadaja;
		this.detaljnoVrijemePocetka = detaljnoVrijemePocetka;
		this.webVrijemePocetka = webVrijemePocetka;
		this.dodatanOpisLige = dodatanOpisLige;
	}

	public String getSport() {
		return sport;
	}

	public void setSport(String sport) {
		this.sport = sport;
	}

	public String getLiga() {
		return liga;
	}

	public void setLiga(String liga) {
		this.liga = liga;
	}

	public String getNazivVrsteTipova() {
		return nazivVrsteTipova;
	}

	public void setNazivVrsteTipova(String nazivVrsteTipova) {
		this.nazivVrsteTipova = nazivVrsteTipova;
	}

	public String getVrsteTipova() {
		return vrsteTipova;
	}

	public void setVrsteTipova(String vrsteTipova) {
		this.vrsteTipova = vrsteTipova;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDatumPrikaza() {
		return datumPrikaza;
	}

	public void setDatumPrikaza(Date datumPrikaza) {
		this.datumPrikaza = datumPrikaza;
	}

	public String getBroj() {
		return broj;
	}

	public void setBroj(String broj) {
		this.broj = broj;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getDobitniTipovi() {
		return dobitniTipovi;
	}

	public void setDobitniTipovi(String dobitniTipovi) {
		this.dobitniTipovi = dobitniTipovi;
	}

	public String getRezultat() {
		return rezultat;
	}

	public void setRezultat(String rezultat) {
		this.rezultat = rezultat;
	}

	public String getDodatanOpisDogadaja() {
		return dodatanOpisDogadaja;
	}

	public void setDodatanOpisDogadaja(String dodatanOpisDogadaja) {
		this.dodatanOpisDogadaja = dodatanOpisDogadaja;
	}

	public String getDetaljnoVrijemePocetka() {
		return detaljnoVrijemePocetka;
	}

	public void setDetaljnoVrijemePocetka(String detaljnoVrijemePocetka) {
		this.detaljnoVrijemePocetka = detaljnoVrijemePocetka;
	}

	public String getWebVrijemePocetka() {
		return webVrijemePocetka;
	}

	public void setWebVrijemePocetka(String webVrijemePocetka) {
		this.webVrijemePocetka = webVrijemePocetka;
	}

	public String getDodatanOpisLige() {
		return dodatanOpisLige;
	}

	public void setDodatanOpisLige(String dodatanOpisLige) {
		this.dodatanOpisLige = dodatanOpisLige;
	}
	
	@Override
	public int hashCode() {
		return this.broj.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Dogadaj)) return false;
		
		Dogadaj other = (Dogadaj) obj;
		
		return (
				this.datumPrikaza.equals(other.datumPrikaza) &&
				this.broj.equals(other.broj)
				);
	}
}