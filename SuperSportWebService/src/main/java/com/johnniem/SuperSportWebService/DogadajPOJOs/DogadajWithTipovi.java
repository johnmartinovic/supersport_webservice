package com.johnniem.SuperSportWebService.DogadajPOJOs;

import java.util.List;

import com.johnniem.SuperSportWebService.Tipovi;

public class DogadajWithTipovi {

	private Dogadaj dogadaj;
	private List<Tipovi> listaTipova;
	
	public DogadajWithTipovi() {
	}
	
	public DogadajWithTipovi(Dogadaj dogadaj, List<Tipovi> listaTipova) {
		this.dogadaj = dogadaj;
		this.listaTipova = listaTipova;
	}
	
	public Dogadaj getDogadaj() {
		return dogadaj;
	}
	
	public void setDogadaj(Dogadaj dogadaj) {
		this.dogadaj = dogadaj;
	}
	
	public List<Tipovi> getListaTipova() {
		return listaTipova;
	}
	
	public void setListaTipova(List<Tipovi> listaTipova) {
		this.listaTipova = listaTipova;
	}

}
