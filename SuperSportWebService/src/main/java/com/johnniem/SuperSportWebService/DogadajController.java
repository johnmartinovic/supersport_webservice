package com.johnniem.SuperSportWebService;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.johnniem.SuperSportWebService.DogadajPOJOs.Dogadaj;

@RestController
public class DogadajController {
	
	@Autowired
	DogadajJDBCDAOImpl DBOps;
	
    @RequestMapping("/getDogadajListByDate")
    public List<Dogadaj> getDogadajListByDate(
    		@RequestParam(value="datumPrikaza") @DateTimeFormat(pattern="yyyy-MM-dd") Date datumPrikaza
    		) {

    	return DBOps.getDogadajList(datumPrikaza);
    }
    
    @RequestMapping("/getDogadajTipoviListById")
    public List<Tipovi> getDogadajTipoviListById(
    		@RequestParam(value="id") int id
    		) {

    	return DBOps.getDogadajTipoviList(id);
    }
	
	/*
    @RequestMapping("/getDogadajInfo")
    public List<Dogadaj> getDogadajInfo(
    		@RequestParam(value="name") String name
    		) {

    	return DBOps.getDogadajInfoByName(name);
    }
    
    @RequestMapping("/getFullDogadajInfo")
    public List<FullDogadaj> getFullDogadajInfo(
    		@RequestParam(value="name") String name
    		) {

    	return DBOps.getFullDogadajInfoByName(name);
    }
	
	@RequestMapping("/getByDate")
    public List<DogadajWithTipovi> getByDate(
    		@RequestParam(value="date") @DateTimeFormat(pattern="yyyy-MM-dd") Date date
    		) {

    	return DBOps.getDogadajWithTipoviInfoByDate(date);
    }
	
    @RequestMapping("/getDogadajiByName")
    public List<DogadajWithTipovi> getDogadajiByName(
    		@RequestParam(value="name") String name
    		) {

    	return DBOps.getDogadajWithTipoviInfoByName(name);
    }
    
    @RequestMapping("/getDogadajFullInfo")
    public DogadajWithTipovi getDogadajFullInfo(
    		@RequestParam(value="datumPrikaza") @DateTimeFormat(pattern="yyyy-MM-dd") Date datumPrikaza,
    		@RequestParam(value="broj") String broj
    		) {

    	return DBOps.getDogadajWithTipovi(datumPrikaza, broj);
    }
    
    */
	
}
