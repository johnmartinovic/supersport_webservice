package com.johnniem.SuperSportWebService.MySqlDAO;

import java.util.Date;
import java.util.List;

import com.johnniem.SuperSportWebService.Tipovi;
import com.johnniem.SuperSportWebService.DogadajPOJOs.Dogadaj;

public interface DogadajJDBCDAO {
	
	public List<Dogadaj> getDogadajList(Date datumPrikaza);
	
	public Dogadaj getDogadaj(Date datumPrikaza, String broj);
	
	public List<Tipovi> getDogadajTipoviList(int dogadajId);
	
	public List<Tipovi> getDogadajTipoviList(Date datumPrikaza, String broj);
	
	/*
	
	public List<DogadajWithTipovi> getDogadajInfoByDate(Date date);
	
	public List<DogadajWithTipovi> getDogadajWithTipoviInfoByName(String naziv);
	
	public List<FullDogadaj> getFullDogadajInfoByName(String naziv);
	
	public List<Dogadaj> getDogadajInfoByName(String naziv);
	
	public List<DogadajWithDogadaji_Id> getDogadajWithIdInfoByName(String naziv);
	
	public List<DogadajWithDogadaji_Id> getDogadajWithIdInfoByDate(Date date);
	
	public List<DogadajWithTipovi> getDogadajWithTipoviInfoByDate(Date date);
	
	public List<Tipovi> getTipoviByDogadajId(int DogadajId);
	*/
	
}
